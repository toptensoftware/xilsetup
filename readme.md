# xilsetup - Xilinx Development Environment Setup Script

xilsetup is a bash script that setups a Xilinx development environment on Ubuntu based Linux machines.  Currently
tested on Ubuntu/Xubuntu 18.04.

It's intended to be run on a brand new installation and installs everything you need to do Xilinx
based development with Xilinx ISE 14.7 (ie: Spartan 6 development) along with some handy tools.

## Usage

After a fresh installation of Ubunut/Xubuntu etc... download the script as follows:

```
> wget https://bitbucket.org/toptensoftware/xilsetup/raw/master/xilsetup
> chmod +x xilsetup
> sudo ./xilsetup --step1
```

This will run "step 1" which will:

* Run apt-get update and upgrade.
* Install a bunch of required tools (screen, python3, pip etc...)
* Install VirtualBox Guest Extensions (if running as a VM under VirtualBox)

After step 1 has finished, reboot and then run "step 2"

```
> sudo ./xilsetup --step2
```

Which will:

* Install Opera
* Install Visual Studio Code
* Install Git
* Install ISE (you'll need to create a Xilinx account and download the installation package)
* Install Xilt - command line tools for running Xilinx builds
* Install mphidflash - tool for flashing devices
* Install Papilio Loader (for Papalio Duo)
* Install Mimas V2 Loader script (and custom firmware for it)

You can opt out of various parts with command line options.  eg: to not install opera:

```
> sudo ./xilsetup --step2 --no-opera
```

Run with --help for other options.

## Installing ISE

xilsetup can launch the ISE installer but can't download it itself (since it requires Xilinx login).

The installation package is expected to be found at the following location:

```
~/Downloads/Xilinx_ISE_DS_Lin_14.7_*.tar
```

If it's not found you'll be prompted to go to the download page.   Login and download it.  When finished,
return to the script and press a key to continue.

If you have a copy of the ISE installation package already downloaded, you can just copy it to the
above location.

If the installation fails for any reason, you can re-run it using `sudo ./xilsetup --ise`.

## Papilio Programmer

If the Papilio tools are installed, the Papilio Loader GUI can be launched as follows:

```
> papilio-loader-gui
```

The Papilio command line programmer can be invoked using:

```
> papilio-prog -h
```


## Mimas V2 Programmer

If the Mimas V2 tools are installed, it assumes you'll be using the replacement firmware [described here](https://github.com/toptensoftware/MimasV2-Loader) (which is [based on this](https://github.com/jimmo/numato-mimasv2-pic-firmware)).

To launch the Mimas V2 programmer, use:

```
> mimasv2-prog --help
```

Note: before using this programmer you'll need to flash the Mimas V2 board with the updated firmware.  See the above
links for how to do this.  Note that the the firmware .hex file can be found in:

```
/opt/MimasV2/
```

Also in that folder you can find the sample Mimas V2 FPGA .bin file.

If you chose for xilsetup to install mphidflash, it'll be available on the path:

```
> mphidflash ...
```

## xilt - Xilinx Command Tools

See here for information about [xilt](https://bitbucket.org/toptensoftware/xilt).

To launch xilt, use:

```
> xilt --help
```

Xilt can be used to run command line builds and for launching various Xilinx GUI tools.


## Visual Studio Code

If you installed Visual Code, it can be launched from the command line.  To work on a particular directory:

```
> code .
```

